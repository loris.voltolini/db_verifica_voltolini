/*
Chiave(codOrdine,CodProd)																	Tabella Normalizzate

Dipendenze
codProd-->DescrizioneProdotto,prezzoUnitario,GiacenzaProdotto (dipendenza parziale)		Prodotto(codProd(chiave),descrizioneProd,prezzoUnitario,Giacenzaprod)
codCliente-->Nominativo (dipendenza parziale)											Clineti(codClienti(chiave),nominativo)	
cod_ordine-->DataOrdine,codClinete (dipendenza parziale)								Ordine(cod_Ordine(chiave),datOrdine,codClinete(chiaveEsterna)
codOrdine,CodProdotto-->prezzoVendita,Quantità 											DettaglioOrdine(codOrdine(chiave),CodProdotto(chiave),PrezzoVendita,QuantitàProd)



1)scrivere un script sql per normalizzare la tabella di partenza. Le tabelle necessarie a
rappresentare questa realtà dovranno riportare gli stessi dati rispettando le indicazioni
della progettazione di database relazionali. Compresi i vincoli di integrità.*/

drop database if exists db_ordiniVerifica;
create database db_ordiniVerifica;
use db_ordiniVerifica;

create table dati(
	cod_ordine varchar(30),
	data_ordine datetime,
	cod_prodotto varchar(10),
	descrizione_prodotto varchar(100),
	prezzo_vendita_prodotto decimal(14,4),
	quantity_prodotto double,
	prezzo_unitario_prodotto decimal(14,4),
	giacenza_prodotto int,
	cod_cliente varchar(10),
	nominativo varchar(100)
);

insert into dati values
('OR01','2021-01-01T09:00','P01','PC xx',1150.00,1,1200.00,9,'C01','Rossi'),
('OR01','2021-01-01T09:00','P02','Stampante',3000.00,1,300.00,19,'C01','Rossi'),
('OR02','2021-01-02T10:00','P01','PC xx',1100.00,2,1200.00,7,'C03','Gialli'),
('OR02','2021-01-02T10:00','P03','Tablet YY',350.00,1,350.00,3,'C03','Gialli'),
('OR03','2021-01-02T14:00','P03','Tablet YY',340.00,1,350.00,2,'C01','Rossi'),
('OR04','2021-01-01T11:00','P01','PC xx',1150.00,1,1200.00,5,'C02','Verdi');

-- Creazione tabelle Per la Normalizzazione dei Dati


-- creazione tabella Clineti(codClienti(chiave),nominativo)	
create table Clienti(
	cod_cliente varchar(10),
	nominativo varchar(100),
    constraint pk_clinete_Clienti primary key(cod_cliente)
);
insert into Clienti select distinct cod_cliente, nominativo from dati order by cod_cliente;
select * from Clienti;

-- creazione tabella Ordine(cod_Ordine(chiave),datOrdine,codClinete(chiaveEsterna)
create table Ordini(
	cod_ordine varchar(30),
	data_ordine datetime,
	cod_cliente varchar(10),
	constraint pk_codOrdine_Ordini primary key(cod_ordine),
    constraint fk_codClienti_clienti foreign key(cod_cliente) references Clienti(cod_cliente)
);
insert into Ordini select distinct cod_ordine, data_ordine,cod_cliente from dati ;
select * from Ordini;

-- tabella Prodotto(codProd(chiave),descrizioneProd,prezzoUnitario,Giacenzaprod)	

create table Prodotto(
	cod_prodotto varchar(10),
	descrizione_prodotto varchar(100),
    prezzo_unitario_prodotto decimal(14,4),
	giacenza_prodotto int,
    constraint pk_codProdotto_Prodotto primary key(cod_prodotto)
);
insert into Prodotto select  cod_prodotto,descrizione_prodotto,prezzo_unitario_prodotto,giacenza_prodotto from dati group by cod_prodotto;
select * from Prodotto;

-- creazione tabella DettaglioOrdine(codOrdine(chiave),CodProdotto(chiave),PrezzoVendita,QuantitàProd)

create table DettagliOrdine(
cod_ordine varchar(10),
cod_prodotto varchar(10),
prezzo_vendita_prodotto decimal(14,4),
quantity_prodotto int,
constraint pk_ordine_prodotto primary key (cod_ordine, cod_prodotto),
constraint fk_ordine_ordini foreign key (cod_ordine) references Ordini(cod_ordine),
constraint fk_prodotto_prodotto foreign key (cod_prodotto) references Prodotto(cod_prodotto)

);
/*
-- 2) creare un trigger che permetta di aggiornare la giacenza di ciascun prodotto all’atto della vendita.
delimiter $$
create trigger update_giagenza after insert on DettagliOrdine
	for each row
	begin
		update Prodotto set Prodotto.giacenza_prodotto=(Prodotto.giagenza_prodotto-new.quantity_prodotto)
			where Prodotto.cod_prodotto=new.cod_prodotto;
	end $$
delimiter ;
*/
insert into DettagliOrdine select distinct cod_ordine,cod_prodotto,prezzo_vendita_prodotto,quantity_prodotto from dati;
select * from DettagliOrdine;


-- 3) scrivere un’istruzione sql che consenta di calcolare l’importo totale per ogni ordine: cod_ordine,data,importo;
select cod_prodotto as Prodotto,descrizione_prodotto as Descrizione
from Prodotto
where cod_prodotto in (select cod_prodotto,Max(count(*))as Conta from DettagliOrdine group by cod_prodotto);

Select tmp.cod_ordine,o.data_ordine as UltimaVendità,sum(tmp.quantity_prodotto*tmp.prezzo_unitario_prodotto)as importo
from ordini o  INNER JOIN (select * from DettagliOrdine d Inner JOIN Prodotto p On d.cod_prodotto=p.cod_prodotto)as tmp On tmp.cod_ordine=o.cod_ordine
where o.data_ordine in (select max(data_ordine) from  ordini  GROUP  by cod_ordine)
GROUP  by tmp.cod_ordine;

-- 4) scrivere un’istruzione sql che consenta di calcolare l’importo totale per ogni cliente: nominativo, importo;

select c.nominativo,sum(tmp.quantity_prodotto*tmp.prezzo_unitario_prodotto)as importo
from Clienti c INNER JOIN (select * from Ordini o INNER JOIN (Select * from DettagliOrdine d INNER JOIN Prodotto p On d.cod_prodotto=p.cod_prodotto)tmp1 On tmp1.cod_ordine=o.cod_ordine) tmp On c.cod_cliente=tmp.cod_cliente
group by c.cod_cliente;


-- 5) scrivere un’istruzione sql che consenta di calcolare l’importo totale per ogni prodotto: cod_prodotto,descrizione,importo;
select p.cod_prodotto,p.descrizione_prodotto,sum(d.quantity_prodotto*p.prezzo_unitario_prodotto)as importo
from Prodotto p INNER JOIN DettagliOrdine d On p.cod_prodotto=d.cod_prodotto
group by p.cod_prodotto;

-- 6) scrivere un’istruzione sql che consenta di trovare il prodotto pìù venduto: cod_prodotto,descrizione;

select p.cod_prodotto as Prodotto,p.descrizione_prodotto as Descrizione
from Prodotto p Inner Join (select count(*)as conta from DettagliOrdine group by cod_prodotto)tmp On tmp.cod_prodotto=o.cod_prodotto
where p.cod_prodotto in max(tmp);
/*
7) scrivere un’istruzione sql che consenta di trovare il cliente con il fatturato maggiore: nominativo,importo;

8)scrivere un’istruzione sql che consenta di trovare il giorno con la frequenza di vendite maggiore: data, frequenza;

*/
